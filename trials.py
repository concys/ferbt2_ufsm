#!/usr/bin/env python
# -*- coding: utf-8 -*-

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#	Copyright (C) 2018  CONCYS - JAIME A. RIASCOS SALAS 
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>

# (C) Jaime A. Riascos, 2018
# jarsalas@inf.ufrgs.br
#
# version 1 (26-sep-2018)

__author__ = "Jaime A. Riascos"



from psychopy import gui, visual, core, data, event, logging, monitors, microphone
from psychopy.constants import (NOT_STARTED, STARTED,
                                STOPPED, FINISHED)
import time
import os
import sys
reload(sys)
sys.setdefaultencoding('utf8')


globalClock = core.Clock()  # to track the time since experiment started
endExpNow = False # flag for 'escape' or other condition => quit the exp


# Experiment information
def setting_exp(expName,expInfo):
    # Path
    _thisDir = os.path.dirname(os.path.abspath(__file__)).decode(sys.getfilesystemencoding())
    os.chdir(_thisDir)
    # Pop-up window
    dlg = gui.DlgFromDict(dictionary=expInfo, title=expName)
    if dlg.OK == False:
        core.quit()  # user pressed cancel
    expInfo['date'] = data.getDateStr()  # add a simple timestamp
    expInfo['expName'] = expName

    # Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
    filename = _thisDir + os.sep + u'data' + os.sep + '%s_%s' % (expInfo['Participant'], expInfo['date'])

    # An ExperimentHandler isn't essential but helps with data saving
    thisExp = data.ExperimentHandler(name=expName, version='',
                                     extraInfo=expInfo, runtimeInfo=None,
                                     originPath=None,
                                     savePickle=True, saveWideText=True,
                                     dataFileName=filename)
    # save a log file for detail verbose info
    logFile = logging.LogFile(filename + '.log', level=logging.WARNING)
    logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

    return thisExp

# Monitor
def setting_monitor(name, distance, expInfo):
    # Setup Monitor
    from win32api import GetSystemMetrics

    mon = monitors.Monitor(name)
    mon.setDistance(distance)
    mon_size = mon.getSizePix()
    if mon_size==None:
        mon_size=[GetSystemMetrics(0),GetSystemMetrics(1)]
    print(mon_size)
    # Setup Window
    win = visual.Window(
        size=mon_size, fullscr=True, screen=0,
        allowGUI=False, allowStencil=False,
        monitor=mon, color='black', colorSpace='rgb',
        blendMode='avg', useFBO=True,
        units='norm')
    expInfo['frameRate'] = win.getActualFrameRate()
    if expInfo['frameRate'] != None:
        frameDur = 1.0 / round(expInfo['frameRate'])
    else:
        frameDur = 1.0 / 60.0  # could not measure, so guess

    return win

# instructions
def instructions_trial(win, text, endExpNow=endExpNow):
    instructPracticeClock = core.Clock()
    instruction_stimuli = visual.TextStim(win=win, name='instr1',
                             text=text, font='Arial',
                             pos=[-0.1, 0], height=0.1, wrapWidth=1.5, ori=0,
                             bold=True, depth=1,
                             color='white', colorSpace='rgb', opacity=1)

    # ------Prepare to start Routine "instructPractice"-------
    t = 0
    instructPracticeClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    # update component parameters for each repeat
    key_instruction = event.BuilderKeyResponse()
    # keep track of which components have finished
    instructPracticeComponents = [instruction_stimuli, key_instruction]
    for thisComponent in instructPracticeComponents:
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED

    # -------Start Routine "instructPractice"-------
    while continueRoutine:
        # get current time
        t = instructPracticeClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame

        # *instr1* updates
        if t >= 0.0 and instruction_stimuli.status == NOT_STARTED:
            # keep track of start time/frame for later
            instruction_stimuli.tStart = t
            instruction_stimuli.frameNStart = frameN  # exact frame index
            instruction_stimuli.setAutoDraw(True)

        # *ready1* updates
        if t >= 0.0 and key_instruction.status == NOT_STARTED:
            # keep track of start time/frame for later
            key_instruction.tStart = t
            key_instruction.frameNStart = frameN  # exact frame index
            key_instruction.status = STARTED
            # keyboard checking is just starting
            event.clearEvents(eventType='keyboard')
        if key_instruction.status == STARTED:
            theseKeys = event.getKeys(keyList=['return'])

            # check for quit:
            if "escape" in theseKeys:
                endExpNow = True
            if len(theseKeys) > 0:  # at least one key was pressed
                # a response ends the routine
                continueRoutine = False

        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in instructPracticeComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished

        # check for quit (the Esc key)
        if endExpNow or event.getKeys(keyList=["escape"]):
            core.quit()

        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()

    # -------Ending Routine "instructPractice"-------
    for thisComponent in instructPracticeComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)

# fixation time
def wait_time(win, thisExp):
    endExpNow = False  # flag for 'escape' or other condition => quit the exp
    routineTimer = core.CountdownTimer()
    wait_clock = core.Clock()
    wait_cross = visual.ShapeStim(
        win=win, name='polygon', vertices='cross',
        size=(0.1, 0.1),
        ori=0, pos=(0, 0),
        lineWidth=1, lineColor=[1, 1, 1], lineColorSpace='rgb',
        fillColor=[1, 1, 1], fillColorSpace='rgb',
        opacity=1, depth=0.0, interpolate=True)
    t = 0
    wait_clock.reset()  # clock
    frameN = -1
    continueRoutine = True
    routineTimer.add(2.000000)
    # update component parameters for each repeat
    # keep track of which components have finished
    waitComponents = [wait_cross]
    for thisComponent in waitComponents:
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED

    # -------Start Routine "wait"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = wait_clock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame

        # *wait_cross* updates
        if t >= 0.0 and wait_cross.status == NOT_STARTED:
            # keep track of start time/frame for later
            wait_cross.tStart = t
            wait_cross.frameNStart = frameN  # exact frame index
            wait_cross.setAutoDraw(True)
        frameRemains = 0.0 + 2 - win.monitorFramePeriod * 0.75  # most of one frame period left
        if wait_cross.status == STARTED and t >= frameRemains:
            wait_cross.setAutoDraw(False)

        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in waitComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished

        # check for quit (the Esc key)
        if endExpNow or event.getKeys(keyList=["escape"]):
            core.quit()

        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()

    # -------Ending Routine "wait"-------
    for thisComponent in waitComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)

    routineTimer.reset()

# core experiment
def experiment_trial_images(win, task, time, thisExp, expInfo, endExpNow=endExpNow):
    trail_clock = core.Clock()
    routineTimer = core.CountdownTimer()
    trials = data.TrialHandler(nReps=1, method='sequential',
                               extraInfo=expInfo, originPath=-1,
                               trialList=data.importConditions(task),
                               seed=None, name='trials')

    # images
    image_stim = visual.ImageStim(win=win, name='image',
                                  image='sin', mask=None,
                                  ori=0, pos=(0, 0), size=(1.5, 1.5),
                                  color=[1, 1, 1], colorSpace='rgb', opacity=1,
                                  flipHoriz=False, flipVert=False,
                                  texRes=128, interpolate=True, depth=0.0)

    thisExp.addLoop(trials)  # add the loop to the experiment
    thisTrial = trials.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
    if thisTrial != None:
        for paramName in thisTrial:
            exec ('{} = thisTrial[paramName]'.format(paramName))

    for thisTrial in trials:
        currentLoop = trials
        # abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
        if thisTrial != None:
            for paramName in thisTrial:
                exec ('{} = thisTrial[paramName]'.format(paramName))

        # ------Prepare to start Routine "trial"-------
        t = 0
        trail_clock.reset()  # clock
        frameN = -1
        continueRoutine = True
        routineTimer.add(time)
        # update component parameters for each repeat
        image_stim.setImage(img)
        key_resp_2 = event.BuilderKeyResponse()
        # keep track of which components have finished
        trialComponents = [image_stim, key_resp_2]
        for thisComponent in trialComponents:
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED

        # -------Start Routine "trial"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = trail_clock.getTime()
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame

            # *image* updates
            if t >= 0.0 and image_stim.status == NOT_STARTED:
                # keep track of start time/frame for later
                image_stim.tStart = t
                image_stim.frameNStart = frameN  # exact frame index
                image_stim.setAutoDraw(True)
            frameRemains = 0.0 + 0.5 - win.monitorFramePeriod * 0.75  # most of one frame period left
            if image_stim.status == STARTED and t >= frameRemains:
                image_stim.setAutoDraw(False)

            # *key_resp_2* updates
            if t >= 0.0 and key_resp_2.status == NOT_STARTED:
                # keep track of start time/frame for later
                key_resp_2.tStart = t
                key_resp_2.frameNStart = frameN  # exact frame index
                key_resp_2.status = STARTED
                # keyboard checking is just starting
                win.callOnFlip(key_resp_2.clock.reset)  # t=0 on next screen flip
                event.clearEvents(eventType='keyboard')
            frameRemains = 0.0 + 0.5 - win.monitorFramePeriod * 0.75  # most of one frame period left
            if key_resp_2.status == STARTED and t >= frameRemains:
                key_resp_2.status = STOPPED
            if key_resp_2.status == STARTED:
                theseKeys = event.getKeys(keyList=['space'])

                # check for quit:
                if "escape" in theseKeys:
                    endExpNow = True
                if len(theseKeys) > 0:  # at least one key was pressed
                    key_resp_2.keys = theseKeys[-1]  # just the last key pressed
                    key_resp_2.rt = key_resp_2.clock.getTime()
                    # was this 'correct'?
                    if (key_resp_2.keys == str(corrAns)) or (key_resp_2.keys == corrAns):
                        key_resp_2.corr = 1
                    else:
                        key_resp_2.corr = 0
                    # a response ends the routine
                    continueRoutine = False

            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in trialComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished

            # check for quit (the Esc key)
            if endExpNow or event.getKeys(keyList=["escape"]):
                core.quit()

            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()

            # -------Ending Routine "trial"-------
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)

            # check responses
        if key_resp_2.keys in ['', [], None]:  # No response was made
            key_resp_2.keys = None
            # was no response the correct answer?!
            if str(corrAns).lower() == 'none':
                key_resp_2.corr = 1  # correct non-response
            else:
                key_resp_2.corr = 0  # failed to respond (incorrectly)
            # store data for trials (TrialHandler)
        trials.addData('key_resp_2.keys', key_resp_2.keys)
        trials.addData('key_resp_2.corr', key_resp_2.corr)
        if key_resp_2.keys != None:  # we had a response
            trials.addData('key_resp_2.rt', key_resp_2.rt)

        wait_time(win, thisExp)
        thisExp.nextEntry()
        routineTimer.reset()


    # get names of stimulus parameters
    if trials.trialList in ([], [None], None):
        params = []
    else:
        params = trials.trialList[0].keys()
    # save data for this loop
    trials.saveAsExcel(thisExp.dataFileName + '.xlsx', sheetName='task',
                       stimOut=params,
                       dataOut=['all_raw'])
    # these shouldn't be strictly necessary (should auto-save)

    logging.flush()


def experiment_trial_video(win,thisExp, expInfo, text_w, endExpNow=endExpNow):

    trialClock = core.Clock()
    routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine
    trials = data.TrialHandler(nReps=1, method='random',
                           extraInfo=expInfo, originPath=-1,
                           trialList=data.importConditions(u'common/conds_3.xlsx'),
                           seed=None, name='trials')

    thisExp.addLoop(trials)  # add the loop to the experiment
    thisTrial = trials.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
    if thisTrial != None:
        for paramName in thisTrial:
            exec ('{} = thisTrial[paramName]'.format(paramName))

    for thisTrial in trials:

        currentLoop = trials
        # abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
        if thisTrial != None:
            for paramName in thisTrial:
                exec ('{} = thisTrial[paramName]'.format(paramName))

        # ------Prepare to start Routine "trial"-------
        t = 0
        trialClock.reset()  # clock
        frameN = -1
        continueRoutine = True
        routineTimer.add(11.000000)
        # update component parameters for each repeat
        movie = visual.MovieStim3(
            win=win, name='movie',
            noAudio=True,
            filename=video,
            ori=0, pos=(0, 0), opacity=1,
            depth=0.0,
        )

        key_resp_2 = event.BuilderKeyResponse()
        # keep track of which components have finished
        trialComponents = [movie, key_resp_2]
        for thisComponent in trialComponents:
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED

        # -------Start Routine "trial"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = trialClock.getTime()
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame

            # *movie* updates
            if t >= 1 and movie.status == NOT_STARTED:
                # keep track of start time/frame for later
                movie.tStart = t
                movie.frameNStart = frameN  # exact frame index
                movie.setAutoDraw(True)
            frameRemains = 1 + 10 - win.monitorFramePeriod * 0.75  # most of one frame period left
            if movie.status == STARTED and t >= frameRemains:
                movie.setAutoDraw(False)

            # *key_resp_2* updates
            if t >= 1 and key_resp_2.status == NOT_STARTED:
                # keep track of start time/frame for later
                key_resp_2.tStart = t
                key_resp_2.frameNStart = frameN  # exact frame index
                key_resp_2.status = STARTED
                # keyboard checking is just starting
                event.clearEvents(eventType='keyboard')
            frameRemains = 1 + 10 - win.monitorFramePeriod * 0.75  # most of one frame period left
            if key_resp_2.status == STARTED and t >= frameRemains:
                key_resp_2.status = STOPPED
            if key_resp_2.status == STARTED:
                theseKeys = event.getKeys(keyList=['space'])

                # check for quit:
                if "escape" in theseKeys:
                    endExpNow = True
                if len(theseKeys) > 0:  # at least one key was pressed
                    # a response ends the routine
                    key_resp_2.keys = theseKeys[-1]  # just the last key pressed
                    key_resp_2.rt = key_resp_2.clock.getTime()
                    continueRoutine = False

            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in trialComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished

            # check for quit (the Esc key)
            if endExpNow or event.getKeys(keyList=["escape"]):
                core.quit()

            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()

        # -------Ending Routine "trial"-------
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)

        if key_resp_2.keys != None:  # we had a response
            trials.addData('key_resp_2.rt', key_resp_2.rt)

        answer_video(win, text_w, trials, answ, thisExp, expInfo)

        thisExp.nextEntry()
        routineTimer.reset()
        logging.flush()

    # get names of stimulus parameters
    if trials.trialList in ([], [None], None):
        params = []
    else:
        params = trials.trialList[0].keys()
    # save data for this loop
    trials.saveAsExcel(thisExp.dataFileName + '.xlsx', sheetName='task',
                       stimOut=params,
                       dataOut=['all_raw'])

    logging.flush()


def answer_video(win, text, trials, answ, thisExp, expInfo, endExpNow=endExpNow):

    microphone.switchOn()
    wavDirName = thisExp.dataFileName + '_wav'
    if not os.path.isdir(wavDirName):
        os.makedirs(wavDirName)  # to hold .wav files
    trialClock = core.Clock()
    routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine

    thisExp = data.ExperimentHandler(name='trials', version='',
                                     extraInfo=expInfo, runtimeInfo=None,
                                     originPath=None,
                                     savePickle=False, saveWideText=False,
                                     dataFileName=thisExp.dataFileName)

    instruction_stimuli = visual.TextStim(win=win, name='instr1',
                             text=text, font='Arial',
                             pos=[-0.1, 0], height=0.1, wrapWidth=1.5, ori=0,
                             bold=True, depth=1,
                             color='white', colorSpace='rgb', opacity=1)

    # ------Prepare to start Routine "instructPractice"-------
    t = 0
    trialClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    # update component parameters for each repeat
    key_instruction = event.BuilderKeyResponse()
    # keep track of which components have finished
    mic_1 = microphone.AdvAudioCapture(name=answ, saveDir=wavDirName, stereo=False)

    instructPracticeComponents = [instruction_stimuli, key_instruction, mic_1]
    for thisComponent in instructPracticeComponents:
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED

    # -------Start Routine "instructPractice"-------
    while continueRoutine:
        # get current time
        t = trialClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame

        # *instr1* updates
        if t >= 0.0 and instruction_stimuli.status == NOT_STARTED:
            # keep track of start time/frame for later
            instruction_stimuli.tStart = t
            instruction_stimuli.frameNStart = frameN  # exact frame index
            instruction_stimuli.setAutoDraw(True)

        # *mic_1* updates
        if t >= 0 and mic_1.status == NOT_STARTED:
            # keep track of start time/frame for later
            mic_1.tStart = t
            mic_1.frameNStart = frameN  # exact frame index
            mic_1.status = STARTED
            mic_1.record(sec=10, block=False)  # start the recording thread

        if mic_1.status == STARTED and not mic_1.recorder.running:
            mic_1.status = FINISHED

        #keyboard updates
        if t >= 0.0 and key_instruction.status == NOT_STARTED:
            # keep track of start time/frame for later
            key_instruction.tStart = t
            key_instruction.frameNStart = frameN  # exact frame index
            key_instruction.status = STARTED
            # keyboard checking is just starting
            event.clearEvents(eventType='keyboard')

        if key_instruction.status == STARTED:
            theseKeys = event.getKeys(keyList=['return'])

            # check for quit:
            if "escape" in theseKeys:
                endExpNow = True
            if len(theseKeys) > 0:  # at least one key was pressed
                # a response ends the routine
                continueRoutine = False

        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in instructPracticeComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished

        # check for quit (the Esc key)
        if endExpNow or event.getKeys(keyList=["escape"]):
            core.quit()
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    # -------Ending Routine "instructPractice"-------
    for thisComponent in instructPracticeComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)

    # mic_1 stop & responses
    mic_1.stop()  # sometimes helpful
    if not mic_1.savedFile:
        mic_1.savedFile = None
        # store data for trials (TrialHandler)
    thisExp.addData(answ, mic_1.savedFile)
    thisExp.nextEntry()
    routineTimer.reset()

