#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#	Copyright (C) 2018  CONCYS - JAIME A. RIASCOS SALAS 
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>

# (C) Jaime A. Riascos, 2018
# jarsalas@inf.ufrgs.br
#
# version 1 (26-sep-2018)

__author__ = "Jaime A. Riascos"


from conditions import creating_conditions
from trials import *

# Creating files for conditions (running once)
# creating_conditions('cond_1')
# creating_conditions('cond_2')

#Texts
text_instructions_1 = 'Bem-vindo ao experimento,\nVocê irá ver diferentes pessoas demonstrando emoções. '\
                      'Você deve pressionar a tecla ESPAÇO no computador SOMENTE, quando identificar expressões de MEDO, TRISTEZA e RAIVA.\n' \
                      'Pressione a tecla o mais rápido que você conseguir, depois que identificar a expressão.\n\n\n' \
                      'Você está pronto? Então aperte ENTER para começar.'


text_instructions_2 = 'Vamos mais uma vez?\n' \
                      'Lembre que você deve pressionar a tecla ESPAÇO no computador SOMENTE, quando identificar expressões de MEDO, TRISTEZA e RAIVA.\n' \
                      'Pressione a tecla o mais rápido que você conseguir, depois que identificar a expressão.\n\n\n' \
                      'Você está pronto? Então aperte ENTER para começar.'

text_instructions_3 = 'Beleza, agora você irá ver alguns vídeos.\n' \
                      'Você deve identificar qual expressão facial a pessoa está apresentando: ALEGRIA, RAIVA, NOJO, TRISTEZA, MEDO ou SURPRESA.\n' \
                      'Pressione a tecla ESPAÇO assim que você souber qual expressão é. Depois, uma tela será apresentada para você falar a expressão identificada.\n\n\n' \
                      'Você está pronto? Então aperte ENTER para começar.'

text_instructions_v = 'Por favor dizer a expressão identificada.\n' \
                      'Depois de falar, pressione a tecla ENTER para ver o próximo vídeo.'

text_thanks = 'Obrigado pela participação. Pressione ENTER para sair.'
# Store info about the experiment session
expName = 'FERBT2'  # from the Builder filename that created this script
expInfo = {u'Session': u'01', u'Participant': u''}
thisExp = setting_exp(expName, expInfo)

# Setup monitor
win = setting_monitor('default', 80, expInfo)

# Instructions trial 1
instructions_trial(win, text_instructions_1)

# #task1
task1 = 'common\conds_1.xlsx'
time1= 0.500
wait_time(win, thisExp) # 2 seconds
experiment_trial_images(win, task1, time1, thisExp, expInfo)

# Instructions trial 2
instructions_trial(win, text_instructions_2)

#task2

task2 = 'common\conds_2.xlsx'
time2= 0.2500
wait_time(win, thisExp)
experiment_trial_images(win, task2, time2,  thisExp, expInfo)

# Instructions trial 3

instructions_trial(win, text_instructions_3)

#task3

experiment_trial_video(win,thisExp, expInfo, text_instructions_v)

# Thanks trial
instructions_trial(win, text_thanks)

thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
