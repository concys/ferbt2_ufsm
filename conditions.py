#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#	Copyright (C) 2018  CONCYS - JAIME A. RIASCOS SALAS 
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>

# (C) Jaime A. Riascos, 2018
# jarsalas@inf.ufrgs.br
#
# version 1 (26-sep-2018)

__author__ = "Jaime A. Riascos"



def creating_conditions(filename):
    import xlsxwriter
    import random
    workbook = xlsxwriter.Workbook('common/'+filename+'.xlsx')
    worksheet = workbook.add_worksheet()
    num=random.sample(range(1,5),4)
    row = 1
    worksheet.write(0, 0, 'img')
    worksheet.write(0, 1, 'corrAns')
    for i in num:
        img = random.sample(['a', 'm', 'n', 'r', 's', 't'], 6)
        for j in img:
            worksheet.write(row, 0, 'common/'+j+str(i)+'.jpg')
            if j=='m' or j=='r' or j=='t':
                worksheet.write(row, 1, 'space')
            row+=1
    workbook.close()